﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class Program
    {
        static void Main(string[] args)
        {
            // Создание и инициализация студента
            Student student = new Student
            {
                Name = "Test",
                Age = 20,
                Height = 180,

                Subjects = new List<Subject>()
                {
                    new Subject("История", 4.92),
                    new Subject("Математика", 4.31),
                    new Subject("Русский язык", 4.75)
                }
            };

            student.PrintStudentInfo();

            Console.ReadKey();
        }
    }
}
