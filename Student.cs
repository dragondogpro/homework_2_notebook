﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Notebook
{
    class Student
    {
        // Поле имени
        private string name;
        // Поле возраста
        private int age;
        // Поле роста
        private double height;
        // Лист предметов с оценками
        private List<Subject> subjects;

        // Методы Get и Set для получения доступа к полю имени
        public string Name { get => name; set => name = value; }
        // Методы Get и Set для получения доступа к полю возраста
        public int Age { get => age; set => age = value; }
        // Методы Get и Set для получения доступа к полю роста
        public double Height { get => height; set => height = value; }
        // Методы Get и Set для получения доступа к списку предметов
        internal List<Subject> Subjects { get => subjects; set => subjects = value; }

        /// <summary>
        /// Метод, возвращающий строку с информацией о студенте и его оценках
        /// </summary>
        /// <returns> Форматированная строка </returns>
        private string GetStudentInfo()
        {
            // Формирование строки с информацией о студенте
            string studentInfo = $"Имя: {name};Возраст: {age};Рост: {height}";

            // Инициализация строки в которую будет записана информация о предметах
            string subjectInfo = ";Оценки:;";

            // Формирование строки с информацией о предметах 
            foreach (Subject subject in Subjects)
                subjectInfo += subject.PrintSubject();

            // Объединение ранее полученных строк в один вывод
            return $"{studentInfo};{subjectInfo};Средний балл: {GetAverageMark()}";
        }

        /// <summary>
        /// Метод получения среднего арифметического из всех оценок
        /// </summary>
        /// <returns>Среднее значение по всем предметам</returns>
        public double GetAverageMark()
        {
            // Переменная для записи результата
            double result = 0;

            // Цикл, для поиска оценок по всем предметам и последующее их сложение
            foreach (Subject subject in subjects)
                result += subject.Mark;

            // Деление суммы всех оценок на их количество и запись
            // результата деления с форматированием до 2х знаков после запятой
            result = Math.Round(result / subjects.Count(), 2);

            // Возвращение результата вычисления среднего значения
            return result;
        }

        public void PrintStudentInfo()
        {
            // Разбиение сформированной строки по спец. символу 
            List<string> studentInfo = GetStudentInfo().Split(';').ToList();
            // Переменная для построчного вывода
            int i = 0;

            // Цикл вывода информации о студенте
            foreach (string info in studentInfo)
            {
                // Указатель на позицию курсора в командной строке
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, i);
                // Вывод информации
                Console.WriteLine(info);
                // Инкремент строки
                i++;
            }
        }
    }
}
