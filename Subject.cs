﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class Subject
    {
        // Поле названия предмета
        private string name;
        // Поле оценки по предмету
        private double mark;

        // Конструктор для объекта 
        public Subject(string _name, double _mark)
        {
            Name = _name;
            Mark = _mark;
        }

        // Методы Get и Set для получения доступа к полю оценки
        public double Mark { get => mark; set => mark = value; }
        // Методы Get и Set для получения доступа к полю названия предмета
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// Метод, возвращающий форматированную строку с предметом
        /// </summary>
        /// <returns> Форматированная строка </returns>
        public string PrintSubject()
        {
            string subjectInfo = string.Format("{0}: {1};", name, mark);
            return subjectInfo;
        }
    }
}
